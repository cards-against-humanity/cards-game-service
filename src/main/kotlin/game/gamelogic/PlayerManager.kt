package game.gamelogic

import model.WhiteCard

class PlayerManager(private val deck: WhiteCardDeck) {
    private val _playersList: MutableList<ManagedPlayer> = ArrayList()
    private val _players: MutableMap<String, ManagedPlayer> = HashMap()

    private val _artificialPlayersList: MutableList<ManagedArtificialPlayer> = ArrayList()
    private val _artificialPlayers: MutableMap<String, ManagedArtificialPlayer> = HashMap()

    private val _queuedArtificialPlayersList: MutableList<ManagedArtificialPlayer> = ArrayList()
    private val _queuedArtificialPlayers: MutableMap<String, ManagedArtificialPlayer> = HashMap()

    private val _queuedPlayersList: MutableList<ManagedPlayer> = ArrayList()
    private val _queuedPlayers: MutableMap<String, ManagedPlayer> = HashMap()

    private val _playerScores: MutableMap<PlayerId, Int> = HashMap() // Contains scores for real and fake players

    val playersList: List<ManagedPlayer> get() { return _playersList }
    val players: Map<String, ManagedPlayer> get() { return _players }
    val artificialPlayersList: List<ManagedArtificialPlayer> get() { return _artificialPlayersList }
    val artificialPlayers: Map<String, ManagedArtificialPlayer> get() { return _artificialPlayers }
    val queuedPlayersList: List<ManagedPlayer> get() { return _queuedPlayersList }
    val queuedPlayers: Map<String, ManagedPlayer> get() { return _queuedPlayers }
    val queuedArtificialPlayersList: List<ManagedArtificialPlayer> get() { return _queuedArtificialPlayersList }
    val queuedArtificialPlayers: Map<String, ManagedArtificialPlayer> get() { return _queuedArtificialPlayers }
    var owner: PlayerGameLogicModel? = null
        private set
    var judge: PlayerGameLogicModel? = null
        private set

    fun addPlayer(playerId: PlayerId) {
        assertNotInGame(playerId)
        if (playerId.isRealUser()) {
            val player = ManagedPlayer(playerId.userId!!)
            if (_players.isEmpty()) {
                owner = player
            }
            _players[playerId.userId] = player
            _playersList.add(player)
        } else {
            val player = ManagedArtificialPlayer(playerId.artificialPlayerUUID!!)
            _artificialPlayers[playerId.artificialPlayerUUID] = player
            _artificialPlayersList.add(player)
        }
        _playerScores[playerId] = 0
    }

    fun addPlayerToQueue(playerId: PlayerId) {
        assertNotInGame(playerId)
        if (playerId.isRealUser()) {
            val player = ManagedPlayer(playerId.userId!!)
            _queuedPlayersList.add(player)
            _queuedPlayers[playerId.userId] = player
        } else {
            val player = ManagedArtificialPlayer(playerId.artificialPlayerUUID!!)
            _queuedArtificialPlayersList.add(player)
            _queuedArtificialPlayers[playerId.artificialPlayerUUID] = player
        }
    }

    fun addQueuedUsers() {
        // toList() creates a non-referenced copy of the ID's
        // which is needed to prevent concurrent modification exceptions
        this._queuedPlayers.keys.toList().forEach {
            removePlayer(PlayerId.fromUserId(it))
            addPlayer(PlayerId.fromUserId(it))
        }
        this._queuedPlayersList.clear()
        this._queuedPlayers.clear()

        this._queuedArtificialPlayers.keys.toList().forEach {
            removePlayer(PlayerId.fromArtificialPlayerName(it))
            addPlayer(PlayerId.fromArtificialPlayerName(it))
        }
        this._queuedArtificialPlayersList.clear()
        this._queuedArtificialPlayers.clear()
    }

    fun removePlayer(playerId: PlayerId) {
        assertInGame(playerId)
        if (playerId.isRealUser()) {
            val userId = playerId.userId!!
            if (_players[userId] == null) {
                _queuedPlayers.remove(userId)
                _queuedPlayersList.removeIf { it.id == userId }
            } else {
                var judgeIndex = _playersList.indexOf(judge)
                _players.remove(userId)
                _playersList.removeIf { it.id == userId }

                // Reassign owner if the current owner is leaving
                if (_players.isEmpty()) {
                    owner = null
                } else if (owner != null && userId == owner!!.id) {
                    owner = _playersList[0]
                }

                // Reassign judge if the current judge is leaving
                if (judge != null && judge!!.id == userId) {
                    if (judgeIndex == _playersList.size) {
                        judgeIndex = 0
                    }
                    judge = if (!_playersList.isEmpty()) {
                        _playersList[judgeIndex]
                    } else {
                        null
                    }
                }

                _playerScores.remove(PlayerId.fromUserId(userId))
            }
        } else {
            val artificialPlayerName = playerId.artificialPlayerUUID!!
            if (_artificialPlayers[artificialPlayerName] == null) {
                _queuedArtificialPlayers.remove(artificialPlayerName)
                _queuedArtificialPlayersList.removeIf { it.name == artificialPlayerName }
            } else {
                _artificialPlayers.remove(artificialPlayerName)
                _artificialPlayersList.removeIf { it.name == artificialPlayerName }

                _playerScores.remove(PlayerId.fromArtificialPlayerName(artificialPlayerName))
            }
        }
    }

    fun nextJudge() {
        if (judge == null) {
            judge = _playersList.shuffled()[0]
        } else {
            var index = _playersList.indexOf(judge) + 1
            if (index == _playersList.size) {
                index = 0
            }
            judge = _playersList[index]
        }
    }

    fun resetJudge() {
        judge = null
    }

    fun resetScores() {
        _playerScores.keys.toList().forEach { _playerScores[it] = 0 }
    }

    fun incrementPlayerScore(id: PlayerId) {
        _playerScores[id] = _playerScores[id]!! + 1
    }

    fun getPlayerScore(id: PlayerId): Int {
        return _playerScores[id]!!
    }

    private fun isInGame(playerId: PlayerId): Boolean {
        if (playerId.isRealUser()) {
            return _players.contains(playerId.userId) || _queuedPlayers.contains(playerId.userId)
        } else {
            return _artificialPlayers.contains(playerId.artificialPlayerUUID) || _queuedArtificialPlayers.contains(playerId.artificialPlayerUUID)
        }
    }

    private fun assertInGame(playerId: PlayerId) {
        if (!isInGame(playerId)) {
            throw Exception("User is not in the game")
        }
    }

    private fun assertNotInGame(playerId: PlayerId) {
        if (isInGame(playerId)) {
            throw Exception("User is already in the game")
        }
    }

    inner class ManagedPlayer(override val id: String) : PlayerGameLogicModel {
        override val score: Int get() { return _playerScores[PlayerId.fromUserId(id)] ?: 0 } // Should only bypass _playerScores if player is in queue
        override val hand: List<WhiteCard> get() { return deck.userHands.getValue(PlayerId.fromUserId(id)) }
    }

    inner class ManagedArtificialPlayer(override val name: String) : ArtificialPlayerGameLogicModel {
        override val score: Int get() { return _playerScores[PlayerId.fromArtificialPlayerName(name)] ?: 0 } // Should only bypass _playerScores if player is in queue
        override val hand: List<WhiteCard> get() { return deck.userHands.getValue(PlayerId.fromArtificialPlayerName(name))
        }
    }
}