package game.gamelogic

import com.fasterxml.jackson.annotation.JsonProperty

// Identifies a real or artificial player uniquely
class PlayerId private constructor(@JsonProperty val userId: String? = null, @JsonProperty val artificialPlayerUUID: String? = null) {

    init {
        if (!(userId == null).xor(artificialPlayerUUID == null)) {
            throw Exception("PlayerId cannot have both a userId and an artificialPlayerUUID")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is PlayerId) {
            return false
        }

        return this.artificialPlayerUUID == other.artificialPlayerUUID && this.userId == other.userId
    }

    override fun hashCode(): Int {
        var result = userId?.hashCode() ?: 0
        result = 31 * result + (artificialPlayerUUID?.hashCode() ?: 0)
        return result
    }

    fun isRealUser(): Boolean {
        return userId != null
    }

    companion object {
        fun fromUserId(userId: String): PlayerId {
            return PlayerId(userId, null)
        }

        fun fromArtificialPlayerName(artificialPlayerName: String): PlayerId {
            return PlayerId(null, artificialPlayerName)
        }
    }
}