package game.gamelogic

import com.fasterxml.jackson.annotation.JsonProperty
import game.InsufficientAccessException
import model.BlackCard
import model.WhiteCard

class GameLogic(private val maxPlayers: Int, private val maxScore: Int, handSize: Int, whiteCards: List<WhiteCard>, blackCards: List<BlackCard> /* TODO - Add socket handler as arg */) {
    companion object {
        private const val minPlayersToStart = 3
        const val maxArtificialPlayers = 5
        private val artificialPlayerDefaultNames = listOf(
                // Greek Gods
                "Dionysus",
                "Asclepius",
                "Hephæstus",
                // My Little Pony Characters
                "Rainbow Dash",
                "Twilight Sparkle",
                "Fluttershy",
                // German Names
                "Hans",
                "Günter",
                "Klaus",
                // Transformers
                "Megatron",
                "Ultra Magnus",
                "Wheeljack",
                // Spies
                "James Bond",
                "Ethan Hunt",
                "Jason Borne",
                // Star Wars Characters
                "Salacious B. Crumb",
                "Logray",
                "HK-47",
                // Ratchet and Clank
                "Captain Quark",
                "Chairman Drek",
                "Mr. Zurkon",
                // Monsters Inc. Characters
                "Mike Wazowski",
                "Henry J. Waternoose III",
                "George Sanderson",
                // Weird Monarch Nicknames
                "Æthelred the Unready",
                "Edward Longshanks",
                "Henry The Accountant",
                // Spongebob Characters
                "Monty P. Moneybags",
                "The Hash Slinging Slasher",
                "Perch Perkins"
        )

        data class PastRoundData(
            val blackCard: BlackCard,
            val whitePlayed: List<Pair<PlayerId, List<WhiteCard>>>,
            val judgeId: String,
            val winnerId: PlayerId
        )
    }

    var roundNonce: Int = 0
        private set

    var stage: GameStage = GameStage.NOT_RUNNING
        private set

    var winnerId: PlayerId? = null
        private set

    private val whiteDeck = WhiteCardDeck(whiteCards, handSize)
    private val blackDeck = BlackCardDeck(blackCards)
    private val playerManager: PlayerManager = PlayerManager(whiteDeck)

    private val isRunning: Boolean
        get() = stage != GameStage.NOT_RUNNING

    val ownerId: String?
        get() = if (playerManager.owner != null) { playerManager.owner!!.id } else { null }

    val judgeId: String?
        get() = if (playerManager.judge != null) { playerManager.judge!!.id } else { null }

    private val _bannedPlayerIds: MutableSet<String> = HashSet()

    val bannedPlayerIds: Set<String>
        get() = _bannedPlayerIds

    val players: Map<String, PlayerGameLogicModel>
        get() = playerManager.players

    val playersList: List<PlayerGameLogicModel>
        get() = playerManager.playersList

    val queuedPlayers: Map<String, PlayerGameLogicModel>
        get() = playerManager.queuedPlayers

    val queuedPlayersList: List<PlayerGameLogicModel>
        get() = playerManager.queuedPlayersList

    val artificialPlayers: Map<String, ArtificialPlayerGameLogicModel>
        get() = playerManager.artificialPlayers

    val artificialPlayersList: List<ArtificialPlayerGameLogicModel>
        get() = playerManager.artificialPlayersList

    val queuedArtificialPlayers: Map<String, ArtificialPlayerGameLogicModel>
        get() = playerManager.queuedArtificialPlayers

    val queuedArtificialPlayersList: List<ArtificialPlayerGameLogicModel>
        get() = playerManager.queuedArtificialPlayersList

    val currentBlackCard: BlackCard?
        get() = if (isRunning) { blackDeck.currentCard } else { null }

    val whitePlayed: Map<PlayerId, List<WhiteCard>>
        get() = whiteDeck.whitePlayed

    // Allows users to view past rounds from the current game
    private val _pastRounds: MutableList<PastRoundData> = ArrayList()
    val pastRounds: List<PastRoundData> get() { return _pastRounds }

    init {
        // TODO - Dynamically decide what the min hand size and min white card count are
        val minCardCount = maxPlayers * (handSize + 4)
        if (handSize < 3) {
            throw Exception("Hand size must be at least 3")
        }
        if (handSize > 20) {
            throw Exception("Hand size cannot be greater than 20")
        }
        if (maxPlayers < minPlayersToStart) {
            throw Exception("Max players must be at least $minPlayersToStart")
        }
        if (maxPlayers > 20) {
            throw Exception("Max players cannot be greater than 20")
        }
        if (whiteCards.size < minCardCount) {
            throw Exception("Not enough white cards, need ${minCardCount - whiteCards.size} more with this many players")
        }
        if (maxScore < 0) {
            throw Exception("Max score cannot be negative")
        }
    }

    @Synchronized fun start(userId: String) {
        when {
            userId != ownerId -> throw InsufficientAccessException("Must be owner to start game")
            isRunning -> throw Exception("Game is already running")
            !hasEnoughPlayersToPlay() -> throw Exception("Must have at least $minPlayersToStart players to start game")
            else -> {
                _pastRounds.clear()
                blackDeck.reset()
                whiteDeck.resetAndDrawNewHands()
                playerManager.resetScores()
                playerManager.nextJudge()
                stage = GameStage.PLAY_PHASE
                playForArtificialPlayers()
                roundNonce++
                winnerId = null
            }
        }
    }

    @Synchronized fun stop(userId: String) {
        if (userId != ownerId) {
            throw InsufficientAccessException("Must be owner to stop game")
        }
        stop()
    }

    private fun stop() {
        if (!isRunning) {
            throw Exception("Game is not running")
        }
        addQueuedUsers()
        playerManager.resetJudge() // TODO - Find a way to move this judge reset to the startGame method so the judge is shown once the game ends
        stage = GameStage.NOT_RUNNING
    }

    @Synchronized fun join(userId: String) {
        if (_bannedPlayerIds.contains(userId)) {
            throw Exception("You are banned from the game")
        }
        if (players.size == maxPlayers) {
            throw Exception("Game is full")
        }

        if (isRunning) {
            playerManager.addPlayerToQueue(PlayerId.fromUserId(userId))
        } else {
            whiteDeck.addUser(PlayerId.fromUserId(userId))
            playerManager.addPlayer(PlayerId.fromUserId(userId))
            removeExcessArtificialPlayers()
        }
    }

    @Synchronized fun leave(userId: String) {
        if (playerManager.players[userId] == null && playerManager.queuedPlayers[userId] == null) {
            throw Exception("User is not in the game")
        }

        if (userId == judgeId && stage != GameStage.ROUND_END_PHASE) { // TODO - Test that played cards are only reverted if round stage is not ROUND_END_PHASE
            whiteDeck.revertPlayedCards()
            stage = GameStage.ROUND_END_PHASE
            roundNonce++
        }
        if (playerManager.players[userId] != null) {
            whiteDeck.removeUser(PlayerId.fromUserId(userId))
        }
        playerManager.removePlayer(PlayerId.fromUserId(userId))
        if (stage == GameStage.PLAY_PHASE && allUsersHavePlayed()) { // Proceed to judge phase if the last remaining player who hasn't played yet leaves the game
            stage = GameStage.JUDGE_PHASE
        }
        if (isRunning && !hasEnoughPlayersToPlay()) {
            stop()
        }
    }

    @Synchronized fun addArtificialPlayer(userId: String, name: String) {
        if (userId != ownerId) {
            throw Exception("Only game owner can add artificial players")
        }
        addArtificialPlayer(name)
    }

    @Synchronized fun removeArtificialPlayer(userId: String, name: String) {
        if (userId != ownerId) {
            throw Exception("Only game owner can remove artificial players")
        }
        removeArtificialPlayer(name)
    }

    @Synchronized fun addArtificialPlayers(userId: String, amount: Int) {
        if (userId != ownerId) {
            throw Exception("Only game owner can add artificial players")
        }
        addArtificialPlayers(amount)
    }

    @Synchronized fun removeArtificialPlayers(userId: String, amount: Int) {
        if (userId != ownerId) {
            throw Exception("Only game owner can remove artificial players")
        }
        removeArtificialPlayers(amount)
    }

    @Synchronized fun kickUser(kickerId: String, kickeeId: String) {
        if (kickerId != ownerId) {
            throw InsufficientAccessException("Must be game owner to kick user")
        } else if (kickerId == kickeeId) {
            throw InsufficientAccessException("Cannot kick yourself from the game")
        }
        leave(kickeeId)
    }

    @Synchronized fun banUser(bannerId: String, banneeId: String) {
        if (bannerId != ownerId) {
            throw InsufficientAccessException("Must be game owner to ban user")
        } else if (bannerId == banneeId) {
            throw InsufficientAccessException("Cannot ban yourself from the game")
        }
        if (players.containsKey(banneeId)) {
            kickUser(bannerId, banneeId)
        }
        _bannedPlayerIds.add(banneeId)
    }

    @Synchronized fun unbanUser(unbannerId: String, unbanneeId: String) {
        if (unbannerId != ownerId) {
            throw Exception("Must be game owner to unban user")
        } else if (!_bannedPlayerIds.contains(unbanneeId)) {
            throw Exception("Cannot unban - user is not banned from the game")
        }
        _bannedPlayerIds.remove(unbanneeId)
    }

    @Synchronized fun playCards(playerId: PlayerId, cardIds: List<String>) {
        if (playerId.isRealUser() && playerId.userId == judgeId) {
            throw Exception("Judge cannot play cards")
        } else if (stage != GameStage.PLAY_PHASE) {
            throw Exception("Cannot play cards right now")
        } else if ((playerId.isRealUser() && players[playerId.userId] == null) || (!playerId.isRealUser() && artificialPlayers[playerId.artificialPlayerUUID] == null)) {
            throw Exception("User is not in the game")
        } else if (cardIds.size != currentBlackCard!!.answerFields) {
            throw Exception("Must play exactly ${currentBlackCard!!.answerFields} cards")
        }

        whiteDeck.playCards(playerId, cardIds)

        if (allUsersHavePlayed()) {
            stage = GameStage.JUDGE_PHASE
        }
    }

    @Synchronized fun playCards(userId: String, cardIds: List<String>) {
        playCards(PlayerId.fromUserId(userId), cardIds)
    }

    @Synchronized fun unPlayCards(userId: String) {
        if (userId == judgeId) {
            throw Exception("Judge cannot un-play cards")
        } else if (stage != GameStage.PLAY_PHASE) {
            throw Exception("Cannot un-play cards right now")
        } else if (players[userId] == null) {
            throw Exception("User is not in the game")
        }

        whiteDeck.unPlayCards(PlayerId.fromUserId(userId))
    }

    @Synchronized fun voteCard(userId: String, cardId: String) {
        if (userId != judgeId) {
            throw Exception("Must be the judge to vote for a card")
        } else if (stage != GameStage.JUDGE_PHASE) {
            throw Exception("Cannot vote for a card")
        } else if (!allUsersHavePlayed()) {
            throw Exception("Not all users have played")
        }

        val winningPlayerId = (whitePlayed.entries.find { it.value.map { it.id }.contains(cardId) } ?: throw Exception("No players have played the specified card")).key
        playerManager.incrementPlayerScore(winningPlayerId)

        winnerId = winningPlayerId
        if (maxScore > 0 && playerManager.getPlayerScore(winningPlayerId) == maxScore) {
            stop()
        } else {
            stage = GameStage.ROUND_END_PHASE
        }
    }

    @Synchronized fun startNextRound() {
        if (stage != GameStage.ROUND_END_PHASE) {
            throw Exception("Cannot start the next round at this time")
        }
        roundNonce++
        // The winnerId should only ever be null here if the judge left during vote phase
        if (winnerId != null) {
            _pastRounds.add(PastRoundData(blackDeck.currentCard, whiteDeck.whitePlayed.toList().filter { it.second.isNotEmpty() }, judgeId!!, winnerId!!))
        }
        winnerId = null
        addQueuedUsers()
        playerManager.nextJudge()
        whiteDeck.discardPlayedCardsAndRedraw()
        blackDeck.setNewCard()
        stage = GameStage.PLAY_PHASE
        playForArtificialPlayers()
    }

    private fun addArtificialPlayer(name: String) {
        if (artificialPlayers.size == maxArtificialPlayers) {
            throw Exception("Cannot add any more artificial players - max is $maxArtificialPlayers")
        }
        if (players.size + artificialPlayers.size == maxPlayers) {
            throw Exception("Cannot add artificial players - Game is full")
        }
        if (isRunning) {
            playerManager.addPlayerToQueue(PlayerId.fromArtificialPlayerName(name))
        } else {
            whiteDeck.addUser(PlayerId.fromArtificialPlayerName(name))
            playerManager.addPlayer(PlayerId.fromArtificialPlayerName(name))
        }
    }

    private fun removeArtificialPlayer(name: String) {
        if (playerManager.artificialPlayers[name] == null && playerManager.queuedArtificialPlayers[name] == null) {
            throw Exception("User is not in the game")
        }
        if (playerManager.artificialPlayers[name] != null) {
            whiteDeck.removeUser(PlayerId.fromArtificialPlayerName(name))
        }
        playerManager.removePlayer(PlayerId.fromArtificialPlayerName(name))
        if (isRunning && !hasEnoughPlayersToPlay()) {
            stop()
        }
    }

    private fun addArtificialPlayers(amount: Int) {
        if (artificialPlayers.size + amount > maxArtificialPlayers) {
            throw Exception("Cannot add that many artificial players")
        }
        val takenNames = (artificialPlayers + queuedArtificialPlayers).values.map { artificialPlayer -> artificialPlayer.name }
        val availableNames = artificialPlayerDefaultNames.filter { !takenNames.contains(it) }
        val chosenNames = availableNames.shuffled().take(amount)
        for (name in chosenNames) {
            addArtificialPlayer(name)
        }
    }

    private fun removeArtificialPlayers(amount: Int) {
        if (amount <= 0) {
            throw Exception("Amount must be a positive number")
        }
        if (amount > artificialPlayers.size + queuedArtificialPlayers.size) {
            throw Exception("Cannot remove that many artificial players - there are only ${artificialPlayers.size + queuedArtificialPlayers.size}")
        }

        val queuedArtificialPlayersToRemove = minOf(amount, queuedArtificialPlayers.size)
        val artificialPlayersToRemove = amount - queuedArtificialPlayersToRemove

        queuedArtificialPlayers.values.map { it.name }.shuffled().take(queuedArtificialPlayersToRemove).forEach { removeArtificialPlayer(it) }
        artificialPlayers.values.map { it.name }.shuffled().take(artificialPlayersToRemove).forEach { removeArtificialPlayer(it) }
    }

    private fun playForArtificialPlayers() {
        artificialPlayersList.forEach { playCards(PlayerId.fromArtificialPlayerName(it.name), it.hand.subList(0, currentBlackCard!!.answerFields).map { it.id }) }
    }

    private fun userHasPlayed(userId: String): Boolean {
        return whitePlayed[PlayerId.fromUserId(userId)]!!.size == currentBlackCard!!.answerFields
    }

    private fun allUsersHavePlayed(): Boolean {
        players.values.map { it.id }.filter { it != judgeId }.forEach {
            if (!userHasPlayed(it)) {
                return false
            }
        }
        return true
    }

    private fun addQueuedUsers() {
        playerManager.queuedPlayers.keys.forEach { whiteDeck.addUser(PlayerId.fromUserId(it)) }
        playerManager.queuedArtificialPlayers.keys.forEach { whiteDeck.addUser(PlayerId.fromArtificialPlayerName(it)) }
        playerManager.addQueuedUsers()
        removeExcessArtificialPlayers()
    }

    private fun removeExcessArtificialPlayers() {
        if (artificialPlayers.size + players.size > maxPlayers) {
            removeArtificialPlayers(artificialPlayers.size + players.size - maxPlayers)
        }
    }

    private fun hasEnoughPlayersToPlay(): Boolean {
        if (players.size < 2) {
            return false
        }
        return players.size + artificialPlayers.size >= minPlayersToStart
    }

    enum class GameStage {
        @JsonProperty("notRunning") NOT_RUNNING,
        @JsonProperty("playPhase") PLAY_PHASE,
        @JsonProperty("judgePhase") JUDGE_PHASE,
        @JsonProperty("roundEndPhase") ROUND_END_PHASE
    }
}