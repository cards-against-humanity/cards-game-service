package game.gamelogic

import model.WhiteCard

interface ArtificialPlayerGameLogicModel {
    val name: String
    val score: Int
    val hand: List<WhiteCard>
}