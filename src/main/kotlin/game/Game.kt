package game

import api.UserFetcher
import game.gamelogic.GameLogic
import game.messaging.GameMessageModule
import game.messaging.MessageModel
import model.BlackCard
import model.FOVArtificialPlayer
import model.FOVGameData
import model.FOVPlayer
import model.GameInfo
import model.PastRound
import model.RealOrArtificialPlayer
import model.WhiteCard
import model.WhitePlayedEntry
import java.io.ByteArrayOutputStream
import java.security.SecureRandom
import java.util.Date

class Game(val name: String, private val maxPlayers: Int, private val maxScore: Int, handSize: Int, whiteCards: List<WhiteCard>, blackCards: List<BlackCard>, private val userFetcher: UserFetcher) {
    companion object {
        private const val messageModuleSize = 100
    }

    private val logic = GameLogic(maxPlayers, maxScore, handSize, whiteCards, blackCards)
    private val messages = GameMessageModule(messageModuleSize)
    private val randomizerSeed: ByteArray = ByteArray(256)
    var lastVote = Date()
        private set
    val playerIds: List<String> get() {
        return logic.players.values.map { it.id }
    }

    init {
        SecureRandom().nextBytes(randomizerSeed)
    }

    private fun getSecureRoundHash(): ByteArray {
        val byteStream = ByteArrayOutputStream()
        byteStream.write(randomizerSeed)
        byteStream.write(logic.roundNonce)
        return byteStream.toByteArray()
    }

    @Synchronized fun start(userId: String) {
        logic.start(userId)
    }

    @Synchronized fun stop(userId: String) {
        logic.stop(userId)
    }

    @Synchronized fun join(userId: String) {
        logic.join(userId)
    }

    @Synchronized fun leave(userId: String) {
        logic.leave(userId)
    }

    @Synchronized fun addArtificialPlayer(userId: String, name: String) {
        logic.addArtificialPlayer(userId, name)
    }

    @Synchronized fun removeArtificialPlayer(userId: String, name: String) {
        logic.removeArtificialPlayer(userId, name)
    }

    @Synchronized fun addArtificialPlayers(userId: String, amount: Int) {
        logic.addArtificialPlayers(userId, amount)
    }

    @Synchronized fun removeArtificialPlayers(userId: String, amount: Int) {
        logic.removeArtificialPlayers(userId, amount)
    }

    @Synchronized fun kickUser(kickerId: String, kickeeId: String) {
        logic.kickUser(kickerId, kickeeId)
    }

    @Synchronized fun banUser(bannerId: String, banneeId: String) {
        logic.banUser(bannerId, banneeId)
    }

    @Synchronized fun unbanUser(unbannerId: String, unbanneeId: String) {
        logic.unbanUser(unbannerId, unbanneeId)
    }

    @Synchronized fun playCards(userId: String, cardIds: List<String>) {
        logic.playCards(userId, cardIds)
    }

    @Synchronized fun unPlayCards(userId: String) {
        logic.unPlayCards(userId)
    }

    @Synchronized fun voteCard(userId: String, cardId: String) {
        logic.voteCard(userId, cardId)
        lastVote = Date()
    }

    @Synchronized fun startNextRound() {
        logic.startNextRound()
    }

    @Synchronized fun isEmpty(): Boolean {
        return logic.players.isEmpty() && logic.queuedPlayers.isEmpty()
    }

    @Synchronized fun addMessage(userId: String, text: String) {
        if (!logic.players.containsKey(userId) && !logic.queuedPlayers.containsKey(userId)) {
            throw Exception("Cannot post message if you are not currently in this game")
        }
        messages.addMessage(MessageModel(userId, text))
    }

    @Synchronized fun getFOV(userId: String): FOVGameData {
        if (!logic.players.keys.contains(userId) && !logic.queuedPlayers.keys.contains(userId)) {
            throw Exception("User is not in this game")
        }

        val requiredUserIds = listOf(
                logic.playersList.map { it.id },
                logic.queuedPlayersList.map { it.id },
                logic.bannedPlayerIds,
                messages.getRecentMessages(messageModuleSize).map { it.userId }
        ).flatten().toMutableSet()
        if (logic.winnerId != null && logic.winnerId!!.isRealUser()) {
            requiredUserIds.add(logic.winnerId!!.userId!!)
        }
        logic.pastRounds.forEach {
            requiredUserIds.add(it.judgeId)
            if (it.winnerId.isRealUser()) {
                requiredUserIds.add(it.winnerId.userId!!)
            }
            it.whitePlayed.map { it.first }.forEach { playerId ->
                if (playerId.isRealUser()) {
                    requiredUserIds.add(playerId.userId!!)
                }
            }
        }
        val users = userFetcher.getUsers(requiredUserIds.toList()).map { it.id to it }.toMap()
        val messages = messages.getRecentMessages(messageModuleSize).map { Message(users[it.userId]!!, it.text) }
        val players = logic.playersList.map { player -> FOVPlayer(player.id, users.getValue(player.id).name, player.score) }
        val artificialPlayers = logic.artificialPlayersList.map { player -> FOVArtificialPlayer(player.name, player.score) }
        val queuedPlayers = logic.queuedPlayersList.map { player -> FOVPlayer(player.id, users.getValue(player.id).name, player.score) } // TODO - Remove queuedPlayers list from playerManager, and have it only keep track of queued user ids
        val queuedArtificialPlayers = logic.queuedArtificialPlayersList.map { player -> FOVArtificialPlayer(player.name, player.score) } // TODO - Remove queuedArtificialPlayers list from playerManager, and have it only keep track of queued artificial user names
        val bannedPlayers = logic.bannedPlayerIds.map { users.getValue(it) }
        val cardsPlayedAnonymous: List<List<WhiteCard>>? = if (logic.stage == GameLogic.GameStage.JUDGE_PHASE) {
            logic.whitePlayed.filter { (!it.key.isRealUser()) || it.key.userId != logic.judgeId }.values.shuffled(SecureRandom(getSecureRoundHash()))
        } else {
            null
        }
        val cardsPlayed: MutableList<WhitePlayedEntry> = ArrayList()

        for (entry in logic.whitePlayed) {
            if (((!entry.key.isRealUser()) || entry.key.userId != logic.judgeId) && entry.value.isNotEmpty()) {
                val playerId = entry.key
                val whiteCards: List<WhiteCard?> = if ((entry.key.isRealUser() && entry.key.userId == userId) || logic.stage == GameLogic.GameStage.ROUND_END_PHASE || (logic.winnerId != null && logic.stage == GameLogic.GameStage.NOT_RUNNING)) {
                    entry.value
                } else {
                    entry.value.map { null }
                }
                cardsPlayed.add(WhitePlayedEntry(
                        if (playerId.isRealUser()) {
                            RealOrArtificialPlayer(users[playerId.userId!!], null)
                        } else {
                            RealOrArtificialPlayer(null, playerId.artificialPlayerUUID!!)
                        }, whiteCards)
                )
            }
        }

        val hand = if (logic.players[userId] == null) {
            null
        } else {
            logic.players[userId]!!.hand
        }

        return FOVGameData(
                name,
                maxPlayers,
                maxScore,
                logic.stage,
                hand,
                players,
                artificialPlayers,
                queuedPlayers,
                queuedArtificialPlayers,
                bannedPlayers,
                logic.judgeId,
                logic.ownerId!!,
                cardsPlayed,
                cardsPlayedAnonymous,
                logic.currentBlackCard,
                if (logic.winnerId == null) {
                    null
                } else {
                    if (logic.winnerId!!.isRealUser()) {
                        RealOrArtificialPlayer(users[logic.winnerId!!.userId!!], null)
                    } else {
                        RealOrArtificialPlayer(null, logic.winnerId!!.artificialPlayerUUID)
                    }
                },
                messages,
                logic.pastRounds.map { round ->
                    PastRound(round.blackCard, round.whitePlayed.map {
                        WhitePlayedEntry(
                                if (it.first.isRealUser()) {
                                    RealOrArtificialPlayer(users[it.first.userId!!], null)
                                } else {
                                    RealOrArtificialPlayer(null, it.first.artificialPlayerUUID!!)
                                }, it.second)
                    }, users[round.judgeId]!!,
                            if (round.winnerId.isRealUser()) {
                                RealOrArtificialPlayer(users[round.winnerId!!.userId!!], null)
                            } else {
                                RealOrArtificialPlayer(null, round.winnerId!!.artificialPlayerUUID)
                            }
                    )
                }
        )
    }

    @Synchronized fun getInfo(): GameInfo {
        return GameInfo(name, logic.players.size, maxPlayers, userFetcher.getUser(logic.ownerId ?: throw Exception("Game is empty")))
    }
}