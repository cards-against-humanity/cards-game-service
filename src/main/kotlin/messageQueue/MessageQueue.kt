package messageQueue

interface MessageQueue {
    fun gameUpdatedForUsers(userIds: List<String>)
    fun gameListUpdated()
}