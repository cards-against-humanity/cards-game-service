package game

import game.gamelogic.GameLogic
import junit.framework.Assert.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.Date
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import messageQueue.MemoryMessageQueue

class GameManagerTest {

    private var registeredUserIds: List<String> = ArrayList()
    private var registeredCardpackIds: List<String> = ArrayList()

    private var userFetcher = MockUserFetcher()
    private var cardFetcher = MockCardFetcher()
    private var messageQueue = MemoryMessageQueue()
    private var gameManager = GameManager(userFetcher, cardFetcher, GameManager.GameManagerOptions(50, 10000), messageQueue)

    @BeforeEach
    fun reset() {
        userFetcher = MockUserFetcher()
        cardFetcher = MockCardFetcher()
        messageQueue = MemoryMessageQueue()

        val userIds: MutableList<String> = ArrayList()
        for (i in 1..100) {
            userIds.add(i.toString())
            userFetcher.setUser(i.toString(), "user_$i")
        }
        registeredUserIds = userIds

        val cardpackIds: MutableList<String> = ArrayList()
        for (i in 1..10) {
            cardpackIds.add(i.toString())
            cardFetcher.setCardpack(i.toString(), listOf(1..100).flatten().map { "whitecard_$it" }, listOf(1..10).flatten().map { "blackcard_$it" }, listOf(1..10).flatten().map { 1 })
        }
        registeredCardpackIds = cardpackIds

        gameManager = GameManager(userFetcher, cardFetcher, GameManager.GameManagerOptions(50, 10000), messageQueue)
    }

    @Test
    fun addsGameToInfoList() {
        val gameName = "game_one"
        val maxPlayers = 4
        gameManager.createGame(registeredUserIds[0], gameName, maxPlayers, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.joinGame(registeredUserIds[1], gameName)

        val infoList = gameManager.getInfoList()
        assertEquals(1, infoList.size)
        assertEquals(gameName, infoList[0].name)
        assertEquals(maxPlayers, infoList[0].maxPlayers)
        assertEquals(registeredUserIds[0], infoList[0].owner.id)
        assertEquals(2, infoList[0].playerCount)
    }

    @Test
    fun removesGameFromInfoListWhenAllUsersLeave() {
        val gameName = "game_one"
        gameManager.createGame(registeredUserIds[0], gameName, 4, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.joinGame(registeredUserIds[1], gameName)
        gameManager.joinGame(registeredUserIds[2], gameName)
        gameManager.joinGame(registeredUserIds[3], gameName)

        gameManager.leaveGame(registeredUserIds[0])
        gameManager.leaveGame(registeredUserIds[1])
        gameManager.leaveGame(registeredUserIds[2])
        gameManager.leaveGame(registeredUserIds[3])

        assertEquals(0, gameManager.getInfoList().size)
    }

    @Test
    fun removeUserFromOldGameWhenCreatingNewOne() {
        val gameOneName = "game_one"
        val gameTwoName = "game_two"
        gameManager.createGame(registeredUserIds[0], gameOneName, 4, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.createGame(registeredUserIds[0], gameTwoName, 4, 6, 6, listOf(registeredCardpackIds[0]))

        val infoList = gameManager.getInfoList()
        assertEquals(1, infoList.size)
        assertEquals(gameTwoName, infoList[0].name)
    }

    @Test
    fun removeUserFromOldGameWhenJoiningNewOne() {
        val gameOneName = "game_one"
        val gameTwoName = "game_two"
        gameManager.createGame(registeredUserIds[0], gameOneName, 4, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.createGame(registeredUserIds[1], gameTwoName, 4, 6, 6, listOf(registeredCardpackIds[0]))

        gameManager.joinGame(registeredUserIds[2], gameOneName)
        gameManager.joinGame(registeredUserIds[2], gameTwoName)

        assertEquals(1, gameManager.getInfoList().find { it.name == gameOneName }!!.playerCount)
        assertEquals(2, gameManager.getInfoList().find { it.name == gameTwoName }!!.playerCount)
    }

    @Test
    fun cannotMakeTwoGamesWithSameName() {
        gameManager.createGame(registeredUserIds[0], "game", 4, 6, 6, listOf(registeredCardpackIds[0]))
        val e = assertThrows(Exception::class.java) { gameManager.createGame(registeredUserIds[1], "game", 4, 6, 6, listOf(registeredCardpackIds[0])) }
        assertEquals("Game already exists with name: game", e.message)
    }

    @Test
    fun canKickPlayerFromGame() {
        gameManager.createGame(registeredUserIds[0], "game", 4, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.joinGame(registeredUserIds[1], "game")
        gameManager.kick(registeredUserIds[0], registeredUserIds[1])
        assertNull(gameManager.getUserFOV(registeredUserIds[1]))
    }

    @Test
    fun canBanAndUnbanPlayerFromGame() {
        gameManager.createGame(registeredUserIds[0], "game", 4, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.joinGame(registeredUserIds[1], "game")
        gameManager.ban(registeredUserIds[0], registeredUserIds[1])
        assertNull(gameManager.getUserFOV(registeredUserIds[1]))
        gameManager.unban(registeredUserIds[0], registeredUserIds[1])
        gameManager.joinGame(registeredUserIds[1], "game")
    }

    @Test
    fun recyclesGames() {
        val garbageCollectIntervalMillis: Long = 10
        val timeBeforeRemovingGameMillis: Long = 500
        val recyclingGameManager = GameManager(userFetcher, cardFetcher, GameManager.GameManagerOptions(garbageCollectIntervalMillis, timeBeforeRemovingGameMillis), messageQueue)
        val gameName = "game"
        recyclingGameManager.createGame(registeredUserIds[0], gameName, 4, 6, 6, listOf(registeredCardpackIds[0]))
        recyclingGameManager.joinGame(registeredUserIds[1], gameName)

        assertNotNull(recyclingGameManager.getUserFOV(registeredUserIds[0]))
        Thread.sleep(timeBeforeRemovingGameMillis - 50)
        assertNotNull(recyclingGameManager.getUserFOV(registeredUserIds[0]))
        Thread.sleep(100)
        assertNull(recyclingGameManager.getUserFOV(registeredUserIds[0]))
    }

    @Test
    fun doesNotRecycleGamesIfInUse() {
        val garbageCollectIntervalMillis: Long = 10
        val timeBeforeRemovingGameMillis: Long = 500
        val recyclingGameManager = GameManager(userFetcher, cardFetcher, GameManager.GameManagerOptions(garbageCollectIntervalMillis, timeBeforeRemovingGameMillis), messageQueue)
        val gameName = "game"

        val startTime = Date()

        val userIds = listOf(registeredUserIds[0], registeredUserIds[1], registeredUserIds[2])
        recyclingGameManager.createGame(userIds[0], gameName, 4, 6, 6, listOf(registeredCardpackIds[0]))
        userIds.subList(1, userIds.size).forEach { recyclingGameManager.joinGame(it, gameName) }

        while (Date().time < startTime.time + timeBeforeRemovingGameMillis + 100) {
            recyclingGameManager.startGame(userIds[0])
            while (recyclingGameManager.getUserFOV(userIds[0])!!.stage != GameLogic.GameStage.NOT_RUNNING) {
                var judgeId: String? = null
                userIds.forEach {
                    val fov = recyclingGameManager.getUserFOV(it)!!
                    if (it == fov.judgeId) {
                        judgeId = it
                    } else {
                        recyclingGameManager.play(it, fov.hand!!.map { it.id }.subList(0, fov.currentBlackCard!!.answerFields))
                    }
                }
                recyclingGameManager.vote(judgeId!!, recyclingGameManager.getUserFOV(judgeId!!)!!.whitePlayedAnonymous!![0][0].id)
                try {
                    recyclingGameManager.startNextRound(judgeId!!)
                } catch (e: Exception) {}
            }
        }

        assertNotNull(recyclingGameManager.getUserFOV(registeredUserIds[0]))
    }

    @Test
    fun cannotJoinGameYouAreAlreadyIn() {
        gameManager.createGame(registeredUserIds[0], "game", 4, 6, 6, listOf(registeredCardpackIds[0]))
        gameManager.joinGame(registeredUserIds[1], "game")
        val e = assertThrows(Exception::class.java) { gameManager.joinGame(registeredUserIds[1], "game") }
        assertEquals("Cannot join a game you are already in", e.message)
    }

    @Test
    fun sendsEventMessagesCorrectly() {
        // These two numbers make it so we don't need to hard-code values
        // when asserting how many events should be in the message queue
        var gameUpdateEvents = 0
        var gameListUpdateEvents = 0

        // Here we're testing every function of GameManager that sends events to the message queue
        // to make sure that each function produces the right number of events
        assertEquals(gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        gameManager.createGame(registeredUserIds[0], "game", 5, 6, 6, listOf(registeredCardpackIds[0]))
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(1, messageQueue.getGameUpdateCalls().last().size)
        gameManager.joinGame(registeredUserIds[1], "game")
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(2, messageQueue.getGameUpdateCalls().last().size)
        gameManager.joinGame(registeredUserIds[2], "game")
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.sendMessage(registeredUserIds[2], "Hi there!")
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.addArtificialPlayer(registeredUserIds[0], "Bert")
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.addArtificialPlayers(registeredUserIds[0], 1)
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.startGame(registeredUserIds[0])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)

        val judgeId = gameManager.getUserFOV(registeredUserIds[0])!!.judgeId!!
        val nonJudgeIds = gameManager.getUserFOV(registeredUserIds[0])!!.players.map { it.id }.filter { it != judgeId }
        gameManager.play(nonJudgeIds[0], gameManager.getUserFOV(nonJudgeIds[0])!!.hand!!.map { it.id }.subList(0, gameManager.getUserFOV(nonJudgeIds[0])!!.currentBlackCard!!.answerFields))
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.unPlay(nonJudgeIds[0])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.play(nonJudgeIds[0], gameManager.getUserFOV(nonJudgeIds[0])!!.hand!!.map { it.id }.subList(0, gameManager.getUserFOV(nonJudgeIds[0])!!.currentBlackCard!!.answerFields))
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.play(nonJudgeIds[1], gameManager.getUserFOV(nonJudgeIds[1])!!.hand!!.map { it.id }.subList(0, gameManager.getUserFOV(nonJudgeIds[1])!!.currentBlackCard!!.answerFields))
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.vote(judgeId, gameManager.getUserFOV(judgeId)!!.whitePlayedAnonymous!![0][0].id)
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.startNextRound(judgeId)
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)

        gameManager.stopGame(registeredUserIds[0])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.removeArtificialPlayer(registeredUserIds[0], "Bert")
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.removeArtificialPlayers(registeredUserIds[0], 1)
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.leaveGame(registeredUserIds[2])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(3, messageQueue.getGameUpdateCalls().last().size)
        gameManager.kick(registeredUserIds[0], registeredUserIds[1])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(2, messageQueue.getGameUpdateCalls().last().size)
        gameManager.joinGame(registeredUserIds[1], "game")
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(2, messageQueue.getGameUpdateCalls().last().size)
        gameManager.ban(registeredUserIds[0], registeredUserIds[1])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(2, messageQueue.getGameUpdateCalls().last().size)
        gameManager.unban(registeredUserIds[0], registeredUserIds[1])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(1, messageQueue.getGameUpdateCalls().last().size)
        gameManager.leaveGame(registeredUserIds[0])
        assertEquals(++gameUpdateEvents, messageQueue.getGameUpdateCalls().size)
        assertEquals(++gameListUpdateEvents, messageQueue.getGameListUpdateCount())
        assertEquals(1, messageQueue.getGameUpdateCalls().last().size)

        // GameManager can also handle when no MessageQueue is provided
        gameManager = GameManager(userFetcher, cardFetcher, GameManager.GameManagerOptions(50, 10000), null) // Null MessageQueue
        gameManager.createGame(registeredUserIds[0], "game", 5, 6, 6, listOf(registeredCardpackIds[0])) // Should not throw error
    }
}