package game.gamelogic

import model.WhiteCard
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class WhiteCardDeckTest {

    private val handSize = 8
    private var cards: MutableList<WhiteCard> = ArrayList()
    private var deck: WhiteCardDeck

    init {
        fillCardList()
        deck = WhiteCardDeck(cards, handSize)
    }

    private fun fillCardList() {
        for (i in 1..100) {
            cards.add(TestWhiteCard(i.toString(), "1", i.toString()))
        }
    }

    @BeforeEach
    fun reset() {
        cards = ArrayList()
        fillCardList()
        deck = WhiteCardDeck(cards, 8)
    }

    @Test
    fun doesNotModifyOriginalList() {
        val cardsCopy = cards.toList()
        val size = cards.size

        for (i in 1..5) {
            deck.addUser(PlayerId.fromUserId(i.toString()))
        }

        assertEquals(size, cards.size)
        assert(cards.toTypedArray().contentDeepEquals(cardsCopy.toTypedArray()))
    }

    @Test
    fun canAddUsersWithoutError() {
        for (i in 1..10) {
            deck.addUser(PlayerId.fromUserId(i.toString()))
        }
    }

    @Test
    fun keepsMapKeysUpToDate() {
        assertEquals(0, deck.userHands.size)
        assertEquals(0, deck.whitePlayed.size)
        deck.addUser(PlayerId.fromUserId("1"))
        assertEquals(1, deck.userHands.size)
        assertEquals(1, deck.whitePlayed.size)
        assertEquals(handSize, deck.userHands[PlayerId.fromUserId("1")]!!.size)
        assertEquals(0, deck.whitePlayed[PlayerId.fromUserId("1")]!!.size)
        deck.addUser(PlayerId.fromUserId("2"))
        assertEquals(2, deck.userHands.size)
        assertEquals(2, deck.whitePlayed.size)
        assertEquals(handSize, deck.userHands[PlayerId.fromUserId("2")]!!.size)
        assertEquals(0, deck.whitePlayed[PlayerId.fromUserId("2")]!!.size)
        deck.removeUser(PlayerId.fromUserId("1"))
        deck.removeUser(PlayerId.fromUserId("2"))
        assertEquals(0, deck.userHands.size)
        assertEquals(0, deck.whitePlayed.size)
    }

    @Test
    fun addsToPlayedCardsMap() {
        deck.addUser(PlayerId.fromUserId("1"))

        val playedIds = deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id }
        deck.playCards(PlayerId.fromUserId("1"), playedIds)
        assertEquals(4, deck.whitePlayed[PlayerId.fromUserId("1")]!!.size)
        assert(deck.whitePlayed[PlayerId.fromUserId("1")]!!.map { it.id }.containsAll(playedIds))
    }

    @Test
    fun removesCardsFromHandWhenPlayingThem() {
        deck.addUser(PlayerId.fromUserId("1"))

        val playedIds = deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id }
        deck.playCards(PlayerId.fromUserId("1"), playedIds)
        assertEquals(handSize - 4, deck.userHands[PlayerId.fromUserId("1")]!!.size)
    }

    @Test
    fun maintainsCardOrderWhenRevertingPlayedCards() {
        deck.addUser(PlayerId.fromUserId("1"))

        val initialHand = deck.userHands[PlayerId.fromUserId("1")]!!.toList()

        deck.playCards(PlayerId.fromUserId("1"), deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id })
        deck.revertPlayedCards()
        assertEquals(handSize, deck.userHands[PlayerId.fromUserId("1")]!!.size)
        deck.userHands[PlayerId.fromUserId("1")]!!.forEachIndexed { index, card -> assertEquals(initialHand[index].id, card.id) }
    }

    @Test
    fun canPlayCardsAfterResettingDeckWithoutError() {
        deck.addUser(PlayerId.fromUserId("1"))
        deck.playCards(PlayerId.fromUserId("1"), deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id })
        deck.resetAndDrawNewHands()
        deck.playCards(PlayerId.fromUserId("1"), deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id })
    }

    @Test
    fun playedCardsAreInTheOrderTheyWerePlayed() {
        deck.addUser(PlayerId.fromUserId("1"))
        deck.addUser(PlayerId.fromUserId("2"))
        val userOnePlayedCardIds = deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id }
        val userTwoPlayedCardIds = deck.userHands[PlayerId.fromUserId("2")]!!.subList(0, 4).map { it.id }.reversed()
        deck.playCards(PlayerId.fromUserId("1"), userOnePlayedCardIds)
        deck.playCards(PlayerId.fromUserId("2"), userTwoPlayedCardIds)
        assertEquals(deck.whitePlayed[PlayerId.fromUserId("1")]!!.map { it.id }, userOnePlayedCardIds)
        assertEquals(deck.whitePlayed[PlayerId.fromUserId("2")]!!.map { it.id }, userTwoPlayedCardIds)
    }

    @Test
    fun canUnPlayCards() {
        deck.addUser(PlayerId.fromUserId("1"))
        val playedCardIds = deck.userHands[PlayerId.fromUserId("1")]!!.subList(0, 4).map { it.id }
        deck.playCards(PlayerId.fromUserId("1"), playedCardIds)
        assertEquals(4, deck.whitePlayed[PlayerId.fromUserId("1")]!!.size)
        deck.unPlayCards(PlayerId.fromUserId("1"))
        assertEquals(0, deck.whitePlayed[PlayerId.fromUserId("1")]!!.size)
    }

    private class TestWhiteCard(
        override val id: String,
        override val cardpackId: String,
        override val text: String
    ) : WhiteCard
}