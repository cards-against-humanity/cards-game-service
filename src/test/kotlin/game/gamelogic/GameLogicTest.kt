package game.gamelogic

import model.BlackCard
import model.WhiteCard
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.math.max
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

class GameLogicTest {

    private val maxScore = 6
    private val maxPlayers = 6
    private var game: GameLogic

    init {
        game = createGame()
    }

    private fun generateWhiteCards(count: Int): List<WhiteCard> {
        val cards: MutableList<WhiteCard> = ArrayList()
        for (i in 1..count) {
            cards.add(TestWhiteCard(i.toString(), "1", i.toString()))
        }
        return cards
    }

    private fun generateBlackCards(count: Int): List<BlackCard> {
        val cards: MutableList<BlackCard> = ArrayList()
        for (i in 1..count) {
            cards.add(TestBlackCard(i.toString(), "1", i.toString(), 1))
        }
        return cards
    }

    private fun createGame(): GameLogic {
        return GameLogic(maxPlayers, maxScore, 6, generateWhiteCards(100), generateBlackCards(100))
    }

    private fun playToEndOfGame() {
        while (game.stage != GameLogic.GameStage.NOT_RUNNING) {
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            game.voteCard(judgeId, nonPlayedCardId)
            if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                game.startNextRound()
            }
        }
    }

    @BeforeEach
    fun reset() {
        game = createGame()
    }

    private fun addUsers() {
        for (i in 1..maxPlayers) {
            game.join(i.toString())
        }
    }

    private fun addUsersAndStartGame() {
        addUsers()
        game.start("1")
    }

    private fun playCardsForAllUsers() {
        game.players.values.forEach {
            if (it.id != game.judgeId) {
                game.playCards(it.id, it.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
            }
        }
    }

    @Test
    fun startAndStopWithoutError() {
        addUsersAndStartGame()
        game.stop("1")
    }

    @Test
    fun errorsWhenStoppingGameThatIsNotRunning() {
        addUsers()
        val e = assertThrows(Exception::class.java, { game.stop("1") })
        assertEquals("Game is not running", e.message)
    }

    @Test
    fun errorsWhenStartingGameThatIsAlreadyRunning() {
        addUsersAndStartGame()
        val e = assertThrows(Exception::class.java) { game.start("1") }
        assertEquals("Game is already running", e.message)
    }

    @Test
    fun cannotAddSameUserTwice() {
        game.join("1")
        val e = assertThrows(Exception::class.java) { game.join("1") }
        assertEquals("User is already in the game", e.message)
    }

    @Test
    fun cannotVoteDuringPlayPhase() {
        addUsersAndStartGame()
        val e = assertThrows(Exception::class.java) { game.voteCard(game.judgeId!!, "") }
        assertEquals("Cannot vote for a card", e.message)
    }

    @Test
    fun gameSetToPlayPhaseWhenStarted() {
        addUsersAndStartGame()
        assertEquals(GameLogic.GameStage.PLAY_PHASE, game.stage)
    }

    @Test
    fun currentBlackCardIsNullWhenGameIsNotRunning() {
        assertNull(game.currentBlackCard)
        addUsersAndStartGame()
        game.stop(game.ownerId!!)
        assertNull(game.currentBlackCard)
    }

    @Test
    fun currentBlackCardIsNotNullWhenGameIsRunning() {
        addUsersAndStartGame()
        assertNotNull(game.currentBlackCard)
    }

    @Test
    fun syncPlayedCardsListsWithUsersInGame() {
        assertTrue(game.whitePlayed.isEmpty())
        game.join("1")
        assertNotNull(game.whitePlayed[PlayerId.fromUserId("1")])
        game.leave("1")
        assertTrue(game.whitePlayed.isEmpty())
    }

    @Test
    fun addsPlayedCards() {
        addUsersAndStartGame()
        val player = game.playersList.find { player -> player.id != game.judgeId }!!
        game.playCards(player.id, player.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        assertEquals(game.currentBlackCard!!.answerFields, game.whitePlayed[PlayerId.fromUserId(player.id)]!!.size)
    }

    @Test
    fun stopsGameWhenPlayerLeavesAndNotEnoughPlayersAreRemaining() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")
        game.leave("3")
        val e = assertThrows(Exception::class.java) { game.stop("1") }
        assertEquals("Game is not running", e.message)
    }

    @Test
    fun queuedPlayersAreAddedIfGameIsStoppedWhileRunning() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")
        playCardsForAllUsers()
        game.join("4")
        game.addArtificialPlayers("1", 1)
        assertEquals(3, game.playersList.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.stop("1")
        assertEquals(4, game.playersList.size)
        assertEquals(1, game.artificialPlayersList.size)
    }

    @Test
    fun canPlayNextRoundWhenJoiningDuringRoundEnd() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")
        playCardsForAllUsers()

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id
        game.voteCard(game.judgeId!!, winningCardId)

        game.join("4")
        game.addArtificialPlayers("1", 1)

        assertEquals(3, game.playersList.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.startNextRound()
        assertEquals(4, game.playersList.size)
        assertEquals(1, game.artificialPlayersList.size)
    }

    @Test
    fun gameEntersJudgePhaseOnceAllUsersHavePlayed() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        playCardsForAllUsers()

        assertEquals(GameLogic.GameStage.JUDGE_PHASE, game.stage)
    }

    @Test
    fun judgeCanVoteAfterAllUsersHavePlayed() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        playCardsForAllUsers()

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id

        game.voteCard(game.judgeId!!, winningCardId)
    }

    @Test
    fun incrementsScoreWhenCardIsVotedFor() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        playCardsForAllUsers()

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winnerId = set.first
        val winningCardId = set.second[0].id

        game.voteCard(game.judgeId!!, winningCardId)
        assertEquals(1, game.players[winnerId.userId]!!.score)
    }

    @Test
    fun returnsPlayedCardsToUsersHandsIfJudgeLeavesDuringPlayPhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start("1")

        val nonJudgeUserId = game.players.values.map { it.id }.find { it != game.judgeId }!!

        val initialHand = game.players[nonJudgeUserId]!!.hand.toList()
        game.playCards(nonJudgeUserId, initialHand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        game.leave(game.judgeId!!)

        val currentHand = game.players[nonJudgeUserId]!!.hand

        assertEquals(initialHand.size, currentHand.size)
        currentHand.forEachIndexed { index, card ->
            assertEquals(initialHand[index].id, card.id)
        }
    }

    @Test
    fun returnsPlayedCardsToUsersHandsIfJudgeLeavesDuringVotePhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start("1")

        val nonJudgeUserId = game.players.values.map { it.id }.find { it != game.judgeId }!!

        val initialHand = game.players[nonJudgeUserId]!!.hand.toList()

        playCardsForAllUsers()

        game.leave(game.judgeId!!)

        val currentHand = game.players[nonJudgeUserId]!!.hand

        assertEquals(initialHand.size, currentHand.size)
        currentHand.forEachIndexed { index, card ->
            assertEquals(initialHand[index].id, card.id)
        }
    }

    @Test
    fun doesNotAutomaticallyRedrawWhenCardIsPlayed() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        val nonJudgeUserId = game.players.values.map { it.id }.find { it != game.judgeId }!!
        val initialHand = game.players[nonJudgeUserId]!!.hand.toList()
        game.playCards(nonJudgeUserId, initialHand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        val currentHand = game.players[nonJudgeUserId]!!.hand
        assertEquals(initialHand.size, currentHand.size + 1)
    }

    @Test
    fun endRoundWhenJudgeLeavesDuringVoteProcess() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start("1")

        playCardsForAllUsers()

        game.leave(game.judgeId!!)

        assertEquals(GameLogic.GameStage.ROUND_END_PHASE, game.stage)
    }

    @Test
    fun gracefullyContinuesWhenJudgeLeavesDuringVotePhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start("1")

        playCardsForAllUsers()
        game.leave(game.judgeId!!)
        game.startNextRound()
        playCardsForAllUsers()

        assertEquals(GameLogic.GameStage.JUDGE_PHASE, game.stage)
    }

    @Test
    fun cannotPlayMoreCardsThanCurrentBlackCardAllows() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        val nonJudgeUserId = game.players.values.map { it.id }.find { it != game.judgeId }!!
        val initialHand = game.players[nonJudgeUserId]!!.hand.toList()
        val e = assertThrows(Exception::class.java) { game.playCards(nonJudgeUserId, initialHand.subList(0, game.currentBlackCard!!.answerFields + 1).map { it.id }) }
        assertEquals("Must play exactly ${game.currentBlackCard!!.answerFields} cards", e.message)
    }

    @Test
    fun cyclesThroughMultipleRoundsWithoutError() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        for (i in 1..1000) {
            if (game.stage == GameLogic.GameStage.NOT_RUNNING) {
                game.start(game.ownerId!!)
            }
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            game.voteCard(judgeId, nonPlayedCardId)
            if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                game.startNextRound()
            }
        }
    }

    @Test
    fun winnerIdIsNullForNewGame() {
        assertNull(game.winnerId)
    }

    @Test
    fun winnerIdIsCorrectWhenGameEnds() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        var winnerId = ""

        while (game.stage != GameLogic.GameStage.NOT_RUNNING) {
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            winnerId = nonJudgePlayerId
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            game.voteCard(judgeId, nonPlayedCardId)
            if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                game.startNextRound()
            }
        }

        assertEquals(winnerId, game.winnerId!!.userId)
    }

    @Test
    fun winnerIdIsNullAfterFinishingGameAndRestarting() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)
        playToEndOfGame()
        game.start(game.ownerId!!)
        assertNull(game.winnerId)
    }

    @Test
    fun winnerIdIsCorrectAfterJudgeHasVoted() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        var winnerId: String? = null

        while (game.stage != GameLogic.GameStage.ROUND_END_PHASE) {
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            game.voteCard(judgeId, nonPlayedCardId)
            winnerId = nonJudgePlayerId
        }

        assertEquals(winnerId, game.winnerId!!.userId)
    }

    @Test
    fun gameStopsAfterMaxScoreIsReached() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        var highestScore = 0

        while (game.stage != GameLogic.GameStage.NOT_RUNNING) {
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            highestScore = Math.max(highestScore, game.players[nonJudgePlayerId]!!.score + 1)
            game.voteCard(judgeId, nonPlayedCardId)
            if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                game.startNextRound()
            }
        }

        assertEquals(maxScore, highestScore)
    }

    @Test
    fun changesJudgeWhenStartingNewRound() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        val judgeId = game.judgeId!!

        playCardsForAllUsers()
        val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
        val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
        val nonPlayedCardId = whitePlayedNonJudge[0].id
        game.voteCard(judgeId, nonPlayedCardId)

        assertEquals(judgeId, game.judgeId)
        game.startNextRound()
        assertNotEquals(judgeId, game.judgeId)
        assertNotNull(game.judgeId)
    }

    @Test
    fun cannotCreateGameWithNegativeMaxScore() {
        val e = assertThrows(Exception::class.java) { GameLogic(maxPlayers, -1, 6, generateWhiteCards(100), generateBlackCards(100)) }
        assertEquals("Max score cannot be negative", e.message)
    }

    @Test
    fun gameCanContinueIfUserJoinsDuringJudgePhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start(game.ownerId!!)

        val judgeId = game.judgeId!!

        playCardsForAllUsers()

        game.join("4")

        val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
        val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
        val nonPlayedCardId = whitePlayedNonJudge[0].id
        game.voteCard(judgeId, nonPlayedCardId)

        assertNull(game.players["4"])
        game.startNextRound()
        assertNotNull(game.players["4"])
    }

    @Test
    fun gameCanContinueIfUserLeavesDuringPlayPhaseWithoutPlayingCards() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        val judgeId = game.judgeId!!
        val nonJudgeLeaverId = game.playersList.find { it.id != game.judgeId }!!.id
        val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId && it.id != nonJudgeLeaverId }!!.id

        game.players.values.forEach {
            if (it.id != game.judgeId && it.id != nonJudgeLeaverId) {
                game.playCards(it.id, it.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
            }
        }

        assertNotNull(game.whitePlayed[PlayerId.fromUserId(nonJudgeLeaverId)])
        game.leave(nonJudgeLeaverId)
        assertNull(game.whitePlayed[PlayerId.fromUserId(nonJudgeLeaverId)])

        val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
        val nonPlayedCardId = whitePlayedNonJudge[0].id
        game.voteCard(judgeId, nonPlayedCardId)
        game.startNextRound()
    }

    @Test
    fun gameCanContinueIfUserLeavesDuringPlayPhaseAfterPlayingCards() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        val nonJudgeLeaver = game.playersList.find { it.id != game.judgeId }!!
        val nonJudgeLeaverId = nonJudgeLeaver.id

        // Play cards and then immediately leave
        game.playCards(nonJudgeLeaverId, nonJudgeLeaver.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        assertNotNull(game.whitePlayed[PlayerId.fromUserId(nonJudgeLeaverId)])
        game.leave(nonJudgeLeaverId)
        assertNull(game.whitePlayed[PlayerId.fromUserId(nonJudgeLeaverId)])

        // Everyone else plays
        game.players.values.forEach {
            if (it.id != game.judgeId && it.id != nonJudgeLeaverId) {
                game.playCards(it.id, it.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
            }
        }

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id
        game.voteCard(game.judgeId!!, winningCardId)

        game.startNextRound()
    }

    @Test
    fun userPlayedCardsAreRemovedIfUserLeavesDuringPlayPhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id

        playCardsForAllUsers()

        assertNotNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
        game.leave(nonJudgePlayerId)
        assertNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
    }

    // TODO - Make this test pass
//    @Test
//    fun judgeCanVoteForUserIfUserHasLeftDuringJudgePhase() {
//        game.join("1")
//        game.join("2")
//        game.join("3")
//        game.join("4")
//        game.start(game.ownerId!!)
//
//        val judgeId = game.judgeId!!
//        val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
//
//        playCardsForAllUsers()
//        game.leave(nonJudgePlayerId)
//
//        assertNotNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
//        val nonPlayedCardId = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!![0].id
//        game.voteCard(judgeId, nonPlayedCardId)
//        game.startNextRound()
//        assertNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
//    }
//
//    @Test
//    fun userIsRemovedFromWhitePlayedWhenGameIsStoppedOrMovesToNextRoundIfTheyLeaveDuringJudgePhase() {
//        // If a user plays cards and the game moves to the judge phase, and then they leave, we need to make sure
//        // that their entry in whitePlayed remains for the rest of the round but is later removed
//        game.join("1")
//        game.join("2")
//        game.join("3")
//        game.join("4")
//        game.start(game.ownerId!!)
//
//        var judgeId = game.judgeId!!
//        var nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
//
//        playCardsForAllUsers()
//        game.leave(nonJudgePlayerId)
//
//        assertNotNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
//        var nonPlayedCardId = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!![0].id
//        game.voteCard(judgeId, nonPlayedCardId)
//        game.startNextRound()
//        assertNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
//
//        // Same test as above, but calling game.stop() instead of game.startNextRound()
//        game.stop(game.ownerId!!)
//        game.start(game.ownerId!!)
//
//        judgeId = game.judgeId!!
//        nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
//
//        playCardsForAllUsers()
//        game.leave(nonJudgePlayerId)
//
//        assertNotNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
//        nonPlayedCardId = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!![0].id
//        game.voteCard(judgeId, nonPlayedCardId)
//        game.stop(game.ownerId!!)
//        assertNull(game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)])
//    }

    @Test
    fun canJoinAndLeaveDuringPlayPhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start(game.ownerId!!)

        game.join("4")
        game.leave("4")
        game.join("4")
        game.leave("4")
        game.join("4")
        game.leave("4")
    }

    @Test
    fun canJoinAndLeaveDuringJudgePhase() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start(game.ownerId!!)

        playCardsForAllUsers()

        game.join("4")
        game.leave("4")
        game.join("4")
        game.leave("4")
        game.join("4")
        game.leave("4")
    }

    @Test
    fun canRejoinAfterBeingKicked() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.kickUser(game.ownerId!!, "4")

        game.join("4")
    }

    @Test
    fun userCannotRejoinAfterBeingBanned() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.banUser(game.ownerId!!, "4")

        val e = assertThrows(Exception::class.java) { game.join("4") }
        assertEquals("You are banned from the game", e.message)
    }

    @Test
    fun userCanRejoinAfterBeingBannedAndUnbanned() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.banUser(game.ownerId!!, "4")
        game.unbanUser(game.ownerId!!, "4")

        game.join("4")
    }

    @Test
    fun canBanUserThatIsNotCurrentlyInGame() {
        game.join("1")
        game.join("2")
        game.join("3")

        game.banUser(game.ownerId!!, "4")

        val e = assertThrows(Exception::class.java) { game.join("4") }
        assertEquals("You are banned from the game", e.message)
    }

    @Test
    fun mustBeOwnerToKickOrBanOrUnban() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        var e = assertThrows(Exception::class.java) { game.kickUser("2", "3") }
        assertEquals("Must be game owner to kick user", e.message)
        e = assertThrows(Exception::class.java) { game.banUser("2", "3") }
        assertEquals("Must be game owner to ban user", e.message)

        game.banUser(game.ownerId!!, "4")
        e = assertThrows(Exception::class.java) { game.unbanUser("2", "4") }
        assertEquals("Must be game owner to unban user", e.message)
    }

    @Test
    fun cannotKickOrBanYourself() {
        game.join("1")
        game.join("2")
        game.join("3")

        var e = assertThrows(Exception::class.java) { game.kickUser(game.ownerId!!, game.ownerId!!) }
        assertEquals("Cannot kick yourself from the game", e.message)
        e = assertThrows(Exception::class.java) { game.banUser(game.ownerId!!, game.ownerId!!) }
        assertEquals("Cannot ban yourself from the game", e.message)
    }

    @Test
    fun cannotUnbanUserThatIsNotBanned() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        var e = assertThrows(Exception::class.java) { game.unbanUser(game.ownerId!!, "2") }
        assertEquals("Cannot unban - user is not banned from the game", e.message)
    }

    @Test
    fun maintainsPlayedCardsWhenGameEnds() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        var lastBlackCard: BlackCard? = null
        var lastJudgeId: String? = null

        while (game.stage != GameLogic.GameStage.NOT_RUNNING) {
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            lastBlackCard = game.currentBlackCard!!
            lastJudgeId = game.judgeId!!
            game.voteCard(judgeId, nonPlayedCardId)
            if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                game.startNextRound()
            }
        }

        game.playersList.map { it.id }.filter { it != lastJudgeId!! }.forEach {
            assertEquals(lastBlackCard!!.answerFields, game.whitePlayed[PlayerId.fromUserId(it)]!!.size)
        }
    }

    @Test
    fun resetsPlayedCardsWhenGameEndsAndRestarts() {
        maintainsPlayedCardsWhenGameEnds() // TODO - Wtf am I doing here? Calling a test from another test?

        game.start(game.ownerId!!)

        game.playersList.map { it.id }.filter { it != game.judgeId!! }.forEach {
            assertEquals(0, game.whitePlayed[PlayerId.fromUserId(it)]!!.size)
        }
    }

    @Test
    fun roundNumberIncrementsWhenEachRoundStarts() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        var roundNum = 0

        for (i in 0..10) {
            game.start(game.ownerId!!)
            roundNum++

            while (game.stage != GameLogic.GameStage.NOT_RUNNING) {
                playCardsForAllUsers()
                val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
                val judgeId = game.judgeId!!
                val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
                val nonPlayedCardId = whitePlayedNonJudge[0].id
                game.voteCard(judgeId, nonPlayedCardId)
                if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                    assertEquals(roundNum++, game.roundNonce)
                    game.startNextRound()
                }
            }
        }
    }

    @Test
    fun retainExistingHandWhenUnsuccessfullyAttemptingToStartGame() {
        game.join("1")
        game.join("2")

        val hand = game.players["1"]!!.hand.toList()

        try {
            game.start("1")
        } catch (e: Exception) {}

        game.players["1"]!!.hand.forEachIndexed { index, whiteCard -> assertEquals(hand[index].id, whiteCard.id) }
    }

    @Test
    fun topQueuedUserBecomesOwnerIfAllCurrentUsersLeave() {
        game.join("1")
        game.join("2")
        game.join("3")

        game.start("1")

        game.join("4")

        game.leave("1")
        game.leave("2")
        game.leave("3")

        assertEquals("4", game.ownerId)
    }

    @Test
    fun queuedUsersAreAddedInOrder() {
        game.join("1")
        game.join("2")
        game.join("3")

        game.start("1")

        game.join("4")
        game.join("5")
        game.join("6")

        game.leave("1")
        game.leave("2")
        game.leave("3")

        assertEquals(3, game.playersList.size)
        assertEquals("4", game.playersList[0].id)
        assertEquals("5", game.playersList[1].id)
        assertEquals("6", game.playersList[2].id)
    }

    @Test
    fun gameCanContinueIfAllCurrentUsersLeave() {
        game.join("1")
        game.join("2")
        game.join("3")

        game.start("1")

        game.join("4")
        game.join("5")
        game.join("6")

        game.leave("1")
        game.leave("2")
        game.leave("3")

        for (i in 1..10) {
            game.start("4")
            playToEndOfGame()
        }
    }

    @Test
    fun canUnPlayBeforeAllUsersHavePlayed() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.start("1")

        val nonJudgeIds = game.players.values.map { it.id }.filter { it != game.judgeId }.toList()
        game.playCards(nonJudgeIds[0], game.players[nonJudgeIds[0]]!!.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        game.playCards(nonJudgeIds[1], game.players[nonJudgeIds[1]]!!.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })

        assert(game.whitePlayed[PlayerId.fromUserId(nonJudgeIds[0])]!!.isNotEmpty())
        game.unPlayCards(nonJudgeIds[0])
        assert(game.whitePlayed[PlayerId.fromUserId(nonJudgeIds[0])]!!.isEmpty())
    }

    @Test
    fun cannotUnPlayAfterAllUsersHavePlayed() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.start("1")

        val nonJudgeIds = game.players.values.map { it.id }.filter { it != game.judgeId }.toList()
        game.playCards(nonJudgeIds[0], game.players[nonJudgeIds[0]]!!.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        game.playCards(nonJudgeIds[1], game.players[nonJudgeIds[1]]!!.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })
        game.playCards(nonJudgeIds[2], game.players[nonJudgeIds[2]]!!.hand.subList(0, game.currentBlackCard!!.answerFields).map { it.id })

        val e = assertThrows(Exception::class.java) { game.unPlayCards(nonJudgeIds[0]) }
        assertEquals("Cannot un-play cards right now", e.message)
    }

    @Test
    fun savesPastRounds() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.start(game.ownerId!!)

        var roundNum = 0

        while (game.stage != GameLogic.GameStage.NOT_RUNNING) {
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            game.voteCard(judgeId, nonPlayedCardId)
            if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
                val blackCard = game.currentBlackCard
                val whitePlayed = game.whitePlayed
                game.startNextRound()
                assertEquals(++roundNum, game.pastRounds.size)
                assertEquals(blackCard, game.pastRounds.last().blackCard)
                assertEquals(whitePlayed.toList().filter { it.first.isRealUser() && it.first.userId!! != judgeId }, game.pastRounds.last().whitePlayed)
            }
        }

        assertEquals(roundNum, game.pastRounds.size)
        game.start(game.ownerId!!)
        assertEquals(0, game.pastRounds.size)
    }

    @Test
    fun keepsPastRoundsWhenGameIsStopped() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")

        game.start(game.ownerId!!)

        playCardsForAllUsers()
        val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
        val judgeId = game.judgeId!!
        val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
        val nonPlayedCardId = whitePlayedNonJudge[0].id
        game.voteCard(judgeId, nonPlayedCardId)
        if (game.stage == GameLogic.GameStage.ROUND_END_PHASE) {
            val blackCard = game.currentBlackCard
            val whitePlayed = game.whitePlayed
            game.startNextRound()
            assertEquals(blackCard, game.pastRounds.last().blackCard)
            assertEquals(whitePlayed.toList().filter { it.first.isRealUser() && it.first.userId!! != judgeId }, game.pastRounds.last().whitePlayed)
        }

        assertEquals(1, game.pastRounds.size)
        game.stop(game.ownerId!!)
        assertEquals(1, game.pastRounds.size)
        game.start(game.ownerId!!)
        assertEquals(0, game.pastRounds.size)
    }

    @Test
    fun endlessModeRunsForever() {
        // Create game with max score of 0 to allow endless mode
        game = GameLogic(maxPlayers, 0, 6, generateWhiteCards(100), generateBlackCards(100))
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)

        for (i in 1..1000) {
            assertEquals(GameLogic.GameStage.PLAY_PHASE, game.stage)
            playCardsForAllUsers()
            val nonJudgePlayerId = game.playersList.find { it.id != game.judgeId }!!.id
            val judgeId = game.judgeId!!
            val whitePlayedNonJudge = game.whitePlayed[PlayerId.fromUserId(nonJudgePlayerId)]!!
            val nonPlayedCardId = whitePlayedNonJudge[0].id
            game.voteCard(judgeId, nonPlayedCardId)
            assertEquals(GameLogic.GameStage.ROUND_END_PHASE, game.stage)
            game.startNextRound()
        }
    }

    @Test
    fun scoresResetWhenGameStarts() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
        game.start(game.ownerId!!)
        playToEndOfGame()
        var highestScore = 0
        game.playersList.forEach {
            highestScore = max(highestScore, it.score)
        }
        assertEquals(maxScore, highestScore)
        game.start(game.ownerId!!)
        game.playersList.forEach { assertEquals(0, it.score) }
    }

    // Artificial Player Tests

    @Test
    fun canAddArtificialPlayersWhenGameIsStopped() {
        val ownerId = "1"
        game.join(ownerId)

        assertEquals(0, game.artificialPlayersList.size)
        game.addArtificialPlayer(ownerId, "Steve")
        assertEquals(game.artificialPlayersList.size, 1)
        game.addArtificialPlayers(ownerId, 1)
        assertEquals(game.artificialPlayersList.size, 2)
    }

    @Test
    fun canAddArtificialPlayersWhenGameIsRunning() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        assertEquals(3, game.whitePlayed.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.addArtificialPlayers("1", 1)
        assertEquals(3, game.whitePlayed.size)
        assertEquals(0, game.artificialPlayersList.size)

        playCardsForAllUsers()

        assertEquals(3, game.whitePlayed.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.addArtificialPlayers("1", 1)
        assertEquals(3, game.whitePlayed.size)
        assertEquals(0, game.artificialPlayersList.size)

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id

        game.voteCard(game.judgeId!!, winningCardId)

        assertEquals(3, game.whitePlayed.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.addArtificialPlayers("1", 1)
        assertEquals(3, game.whitePlayed.size)
        assertEquals(0, game.artificialPlayersList.size)

        game.startNextRound()

        assertEquals(6, game.whitePlayed.size)
        assertEquals(game.artificialPlayersList.size, 3)
    }

    @Test
    fun cannotAddTooManyArtificialPlayers() {
        val ownerId = "1"
        game.join(ownerId)
        game.addArtificialPlayers(ownerId, GameLogic.maxArtificialPlayers)
        var e = assertThrows(Exception::class.java) { game.addArtificialPlayer(ownerId, "Fake Name") }
        assertEquals("Cannot add any more artificial players - max is 5", e.message)

        reset()

        game.join(ownerId)
        for (i in 1..GameLogic.maxArtificialPlayers) {
            game.addArtificialPlayer(ownerId, i.toString())
        }
        e = assertThrows(Exception::class.java) { game.addArtificialPlayer(ownerId, "Fake Name") }
        assertEquals("Cannot add any more artificial players - max is 5", e.message)
    }

    @Test
    fun cannotAddTwoArtificialPlayersWithSameName() {
        game.join("1")
        game.addArtificialPlayer("1", "Steve")
        val e = assertThrows(Exception::class.java) { game.addArtificialPlayer("1", "Steve") }
        assertEquals("User is already in the game", e.message)
    }

    @Test
    fun canAddAndRemoveArtificialPlayer() {
        val ownerId = "1"
        game.join(ownerId)
        assertEquals(0, game.artificialPlayers.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.addArtificialPlayer(ownerId, "Steve")
        assertEquals(1, game.artificialPlayers.size)
        assertEquals(1, game.artificialPlayersList.size)
        game.removeArtificialPlayer(ownerId, "Steve")
        assertEquals(0, game.artificialPlayers.size)
        assertEquals(0, game.artificialPlayersList.size)
        game.addArtificialPlayer(ownerId, "Steve")
    }

    @Test
    fun artificialPlayersPlayAutomatically() {
        game.join("1")
        game.join("2")
        game.addArtificialPlayer("1", "Steve")

        assertTrue(game.whitePlayed[PlayerId.fromArtificialPlayerName("Steve")]!!.isEmpty())
        game.start("1")
        assertFalse(game.whitePlayed[PlayerId.fromArtificialPlayerName("Steve")]!!.isEmpty())
        playCardsForAllUsers()

        assertEquals(GameLogic.GameStage.JUDGE_PHASE, game.stage)
        assertFalse(game.whitePlayed[PlayerId.fromArtificialPlayerName("Steve")]!!.isEmpty())

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id
        game.voteCard(game.judgeId!!, winningCardId)
        game.startNextRound()
        assertFalse(game.whitePlayed[PlayerId.fromArtificialPlayerName("Steve")]!!.isEmpty())

        playCardsForAllUsers()
    }

    @Test
    fun canPlayRoundWithOnlyTwoRealPlayers() {
        game.join("1")
        game.join("2")
        game.addArtificialPlayer("1", "Steve")

        game.start("1")

        assertEquals(3, game.whitePlayed.size)

        playCardsForAllUsers()

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id

        game.voteCard(game.judgeId!!, winningCardId)
    }

    @Test
    fun cannotStartGameWithOnlyOneRealPlayer() {
        game.join("1")
        game.addArtificialPlayer("1", "Steve")
        game.addArtificialPlayer("1", "Robert")

        val e = assertThrows(Exception::class.java, { game.start("1") })
        assertEquals("Must have at least 3 players to start game", e.message)
    }

    @Test
    fun onlyGameOwnerCanAddOrRemoveArtificialPlayers() {
        game.join("1")
        game.join("2")

        var e = assertThrows(Exception::class.java, { game.addArtificialPlayer("2", "Steve") })
        assertEquals("Only game owner can add artificial players", e.message)
        game.addArtificialPlayer("1", "Steve")

        e = assertThrows(Exception::class.java, { game.removeArtificialPlayer("2", "Steve") })
        assertEquals("Only game owner can remove artificial players", e.message)
        game.removeArtificialPlayer("1", "Steve")

        e = assertThrows(Exception::class.java, { game.addArtificialPlayers("2", 3) })
        assertEquals("Only game owner can add artificial players", e.message)
        game.addArtificialPlayers("1", 3)

        e = assertThrows(Exception::class.java, { game.removeArtificialPlayers("2", 3) })
        assertEquals("Only game owner can remove artificial players", e.message)
        game.removeArtificialPlayers("1", 3)
    }

    @Test
    fun cannotRemoveMoreArtificialPlayersThanAreInTheGame() {
        game.join("1")

        game.addArtificialPlayers("1", 3)

        val e = assertThrows(Exception::class.java, { game.removeArtificialPlayers("1", 4) })
        assertEquals("Cannot remove that many artificial players - there are only 3", e.message)
    }

    @Test
    fun cannotAddArtificialPlayersIfGameIsFull() {
        addUsers()
        var e = assertThrows(Exception::class.java, { game.addArtificialPlayers(game.ownerId!!, 1) })
        assertEquals("Cannot add artificial players - Game is full", e.message)
        e = assertThrows(Exception::class.java, { game.addArtificialPlayer(game.ownerId!!, "Rob") })
        assertEquals("Cannot add artificial players - Game is full", e.message)
    }

    @Test
    fun removesArtificialPlayersIfRealPlayerJoinsAndGameIsFull() {
        // Since artificial players count towards the player limit, we need to make sure
        // that real players can take the place of artificial ones without any user intervention
        for (i in 1..maxPlayers - 1) {
            game.join(i.toString())
        }
        game.addArtificialPlayers(game.ownerId!!, 1)
        assertEquals(1, game.artificialPlayers.size)
        game.join(maxPlayers.toString())
        assertEquals(0, game.artificialPlayers.size)

        // Test the same thing, only while the game is running
        reset()

        for (i in 1..maxPlayers - 1) {
            game.join(i.toString())
        }
        game.addArtificialPlayers(game.ownerId!!, 1)
        game.start(game.ownerId!!)
        game.join(maxPlayers.toString())
        playCardsForAllUsers()

        val set = game.whitePlayed.toList().find { it.first.userId != game.judgeId }!!
        val winningCardId = set.second[0].id
        game.voteCard(game.judgeId!!, winningCardId)
        assertEquals(1, game.artificialPlayers.size)
        game.startNextRound()
        assertEquals(0, game.artificialPlayers.size)

        // Test the same thing, only when game is stopped in the middle
        reset()

        for (i in 1..maxPlayers - 1) {
            game.join(i.toString())
        }
        game.addArtificialPlayers(game.ownerId!!, 1)
        game.start(game.ownerId!!)
        game.join(maxPlayers.toString())
        assertEquals(1, game.artificialPlayers.size)
        game.stop(game.ownerId!!)
        assertEquals(0, game.artificialPlayers.size)
    }

    private class TestWhiteCard(
        override val id: String,
        override val cardpackId: String,
        override val text: String
    ) : WhiteCard

    private class TestBlackCard(
        override val id: String,
        override val cardpackId: String,
        override val text: String,
        override val answerFields: Int
    ) : BlackCard
}