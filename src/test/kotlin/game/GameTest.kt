package game

import api.UserFetcher
import game.gamelogic.GameLogic
import model.BlackCard
import model.WhiteCard
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class GameTest {

    private val gameName = "game_name"
    private val maxPlayers = 4
    private val maxScore = 6

    private var game: Game
    private var userFetcher: MockUserFetcher

    init {
        userFetcher = MockUserFetcher()
        game = createGame(userFetcher)
    }

    private fun createGame(fetcher: UserFetcher): Game {
        val whiteCards: MutableList<WhiteCard> = ArrayList()
        val blackCards: MutableList<BlackCard> = ArrayList()
        for (i in 1..100) {
            whiteCards.add(TestWhiteCard(i.toString(), "1", i.toString()))
        }
        for (i in 1..100) {
            blackCards.add(TestBlackCard(i.toString(), "1", i.toString(), 1))
        }
        return Game(gameName, maxPlayers, maxScore, 6, whiteCards, blackCards, fetcher)
    }

    @BeforeEach
    fun reset() {
        userFetcher = MockUserFetcher()

        for (i in 1..100) {
            userFetcher.setUser(i.toString(), "user_$i")
        }

        game = createGame(userFetcher)
    }

    private fun addUsers() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.join("4")
    }

    @Test
    fun fovIncludesSelfInPlayerList() {
        game.join("1")
        game.join("2")
        assert(game.playerIds.contains("1"))
        assert(game.playerIds.contains("2"))
    }

    @Test
    fun fowIncludesAllPlayedCardsByUserOnceRoundIsOver() {
        val userIds: MutableList<String> = mutableListOf("1", "2", "3", "4")

        userIds.forEach { game.join(it) }
        game.start(userIds[0])

        userIds.forEach {
            val fov = game.getFOV(it)
            if (fov.judgeId != it) {
                game.playCards(it, fov.hand!!.subList(0, fov.currentBlackCard!!.answerFields).map { it.id })
            }
        }

        val judgeFov = game.getFOV(game.getFOV(userIds[0]).judgeId!!)

        game.voteCard(judgeFov.judgeId!!, judgeFov.whitePlayedAnonymous!![0][0].id)

        val nonJudgeFov = game.getFOV(userIds.find { it != game.getFOV(it).judgeId }!!)

        assertEquals(userIds.size - 1, nonJudgeFov.whitePlayed.size)
        nonJudgeFov.whitePlayed.forEach {
            assertEquals(judgeFov.currentBlackCard!!.answerFields, it.cards.size)
            it.cards.forEach { assertNotNull(it) }
        }
    }

    @Test
    fun fowShowsAllPlayedCardsByUserAsNullBeforeJudgeVotes() {
        val userIds: MutableList<String> = mutableListOf("1", "2", "3", "4")

        userIds.forEach { game.join(it) }
        game.start(userIds[0])

        userIds.forEach {
            val fov = game.getFOV(it)
            if (fov.judgeId != it) {
                game.playCards(it, fov.hand!!.subList(0, fov.currentBlackCard!!.answerFields).map { it.id })
            }
        }

        val nonJudgeUserId = userIds.find { it != game.getFOV(it).judgeId }!!
        val nonJudgeFov = game.getFOV(nonJudgeUserId)

        assertEquals(userIds.size - 1, nonJudgeFov.whitePlayed.size)
        nonJudgeFov.whitePlayed.forEach {
            assertEquals(nonJudgeFov.currentBlackCard!!.answerFields, it.cards.size)
            if (it.player.user != null && it.player.user!!.id == nonJudgeUserId) {
                it.cards.forEach { assertNotNull(it) }
            } else {
                it.cards.forEach { assertNull(it) }
            }
        }
    }

    @Test
    fun fovHasShowsPlayedCardsForUsersThatPlayedAfterGameHasEnded() {
        val userIds: MutableList<String> = mutableListOf("1", "2", "3", "4")

        userIds.forEach { game.join(it) }
        game.start(userIds[0])

        while (true) {
            userIds.forEach {
                val fov = game.getFOV(it)
                if (fov.judgeId != it) {
                    game.playCards(it, fov.hand!!.subList(0, fov.currentBlackCard!!.answerFields).map { it.id })
                }
            }

            game.voteCard(game.getFOV(userIds[0]).judgeId!!, game.getFOV(userIds[0]).whitePlayedAnonymous!![0][0].id)
            if (game.getFOV(userIds[0]).stage == GameLogic.GameStage.NOT_RUNNING) {
                break
            } else {
                game.startNextRound()
            }
        }

        println(game.getFOV(userIds[0]).whitePlayed)
        assertEquals(userIds.size - 1, game.getFOV(userIds[0]).whitePlayed.size)
        game.getFOV(userIds[0]).whitePlayed.forEach { assertTrue(it.cards.isNotEmpty()) }
    }

    @Test
    fun cannotGetFOVForUserThatIsNotInGame() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        val e = assertThrows(Exception::class.java) { game.getFOV("4") }
        assertEquals("User is not in this game", e.message)
    }

    @Test
    fun userCanRetrieveNonNullFOVWhenJoiningAnInProgressGame() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        game.join("4")
        assertNotNull(game.getFOV("4"))
    }

    @Test
    fun userFOVHandIsNullIfJoiningAnInProgressGame() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        game.join("4")
        assertNull(game.getFOV("4").hand)
    }

    @Test
    fun queuedUserCanSendMessage() {
        game.join("1")
        game.join("2")
        game.join("3")
        game.start("1")

        game.join("4")
        game.addMessage("4", "Hey there!")

        val messages = game.getFOV("4").messages
        assertEquals(1, messages.size)
        assertEquals("Hey there!", messages[0].text)
    }

    @Test
    fun canAddArtificialPlayersInTheMiddleOfAGame() {
        game.join("1")
        game.join("2")
        game.addArtificialPlayer("1", "Steve")
        game.start("1")
        game.addArtificialPlayer("1", "John")

        val artificialPlayers = game.getFOV("1").artificialPlayers
        val queuedArtificialPlayers = game.getFOV("1").queuedArtificialPlayers

        assertEquals(1, artificialPlayers.size)
        assertEquals(1, queuedArtificialPlayers.size)
    }

    @Test
    fun removesQueuedArtificialPlayersFirst() {
        game.join("1")
        game.join("2")
        game.addArtificialPlayer("1", "Steve")
        game.start("1")
        game.addArtificialPlayer("1", "John")
        game.removeArtificialPlayers("1", 1)

        val fov = game.getFOV("1")
        assertEquals(fov.stage, GameLogic.GameStage.PLAY_PHASE) // Should have removed queued artificial user, so the game should not stop

        val artificialPlayers = fov.artificialPlayers
        val queuedArtificialPlayers = fov.queuedArtificialPlayers

        assertEquals(1, artificialPlayers.size)
        assertEquals(0, queuedArtificialPlayers.size)
        assertEquals("Steve", artificialPlayers[0].name)
    }

    private class TestWhiteCard(
        override val id: String,
        override val cardpackId: String,
        override val text: String
    ) : WhiteCard

    private class TestBlackCard(
        override val id: String,
        override val cardpackId: String,
        override val text: String,
        override val answerFields: Int
    ) : BlackCard
}